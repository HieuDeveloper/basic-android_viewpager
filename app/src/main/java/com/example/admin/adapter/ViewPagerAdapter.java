package com.example.admin.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.admin.hocviewpager.R;

/**
 * Created by admin on 9/4/2017.
 */

public class ViewPagerAdapter extends PagerAdapter {

    Context c;
    int[] image;

    public ViewPagerAdapter(Context c, int[] image) {
        this.c = c;
        this.image = image;
    }

    // method trả về số lượng phần tử mảng data hay giờ nó là số lượng view cần hiển thị lên ViewPager.
    @Override
    public int getCount() {
        return image.length;
    }

    //là method tạo page cho từng position nhất định và tái sử dụng page theo từng key (position) của mảng data mResources[].
    // Nó tương tự các method getView hay bindViewHolder trong ListViewAdapter hoặc RecyclerViewAdapter.
    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View trang=LayoutInflater.from(c).inflate(R.layout.item_viewpager,container,false);
        ImageView anh=trang.findViewById(R.id.imgPager);
        anh.setImageResource(image[position]);
        container.addView(trang);
        return trang;
    }

    //Xóa page từ ViewPager, ngoài ra ta có thể sử dụng hàm removeView() để xóa đi object
    // hoặc removeViewAt() để xóa object dựa trên position..
    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    //trả về object dựa trên instantiateItem đang hiển thị dựa vào position...
    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }
}
