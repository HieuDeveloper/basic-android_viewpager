package com.example.admin.hocviewpager;

import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.admin.adapter.ViewPagerAdapter;

public class MainActivity extends AppCompatActivity {
    ViewPager viewPager;
    int[] imageResoure = {
            R.drawable.pinterest,
            R.drawable.telegram,
            R.drawable.twitter,
            R.drawable.youtube
    };
    // View cũng sử dụng Adapter
    ViewPagerAdapter viewPagerAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        viewPager = (ViewPager) findViewById(R.id.viewPager);
        viewPagerAdapter=new ViewPagerAdapter(this,imageResoure);
        viewPager.setAdapter(viewPagerAdapter);
    }
}
